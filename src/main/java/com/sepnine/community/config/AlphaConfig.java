package com.sepnine.community.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;

/**
 * @author Sepnine
 * @version 1.0
 * @description: 配置类
 * @date 2022/9/10 17:10
 */
@Configuration
public class AlphaConfig {

    //定义第三方的Bean
    //@Bean注解 表示该方法的返回值被装入到容器中
    @Bean
    public SimpleDateFormat simpleDateFormat(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }
}
