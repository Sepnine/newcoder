package com.sepnine.community;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @SpringBootApplication: 该注解内含有@SpringBootConfiguration,@EnableAutoConfiguration
 * 标识该类是一个配置类，而且使用了自动配置
 */

/**
 * @description: TODO
 * @author Sepnine
 * @date 2022/9/10 14:55
 * @version 1.0
 */
@SpringBootApplication
public class CommunityApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommunityApplication.class, args);
	}
}
