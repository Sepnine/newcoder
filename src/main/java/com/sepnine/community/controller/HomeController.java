package com.sepnine.community.controller;

import com.sepnine.community.entity.DiscussPost;
import com.sepnine.community.entity.MyPage;
import com.sepnine.community.entity.User;
import com.sepnine.community.service.DiscussPostService;
import com.sepnine.community.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/12 21:11
 */
//不加访问路径
@Controller
public class HomeController {

    @Autowired
    private DiscussPostService discussPostService;

    @Autowired
    private UserService userService;


    /**
     * 返回值需要经过视图解析器的时候，返回String和返回ModelAndView的效果是一样的
     * 只是在使用ModelAndView的 时候，需要使用setViewName方法设置要渲染的html
     */
    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String getIndexPage(Model model, MyPage page){
        // 方法调用钱,SpringMVC会自动实例化Model和Page,并将Page注入Model,但是注入的Page的名称是根据类名来的
        //比如该题注入的是myPage
        // 所以,在thymeleaf中可以直接访问Page对象中的数据.
        page.setRows(discussPostService.findDiscussPostRows(0));
        page.setPath("/index");
        List<DiscussPost> list = discussPostService.findDiscussPosts(0, page.getOffset(), page.getLimit());
        List<Map<String,Object>> discussPosts=new ArrayList<>();
        if(list!=null){
            for(DiscussPost post:list){
                Map<String ,Object> map=new HashMap<>();
                map.put("post",post);
                User user = userService.findUserById(post.getUserId());
                map.put("user",user);
                discussPosts.add(map);
            }
        }
        model.addAttribute("discussPosts",discussPosts);
        model.addAttribute("page",page);
        return "index";
    }

}
