package com.sepnine.community.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/12 20:11
 */
@Data
public class DiscussPost {
    private int id;
    private int userId;
    private String title;
    private String content;
    private int type;
    private int status;
    private Date createTime;
    private int commentCount;
    private double score;
}
