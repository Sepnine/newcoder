package com.sepnine.community.dao;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/10 16:53
 */
@Repository
@Primary //对于同类型的Bean，该类有更高的优先级
public class AlphaDaoMybatisImpl implements AlphaDao{
    @Override
    public String select() {
        return "Mybatis";
    }
}
