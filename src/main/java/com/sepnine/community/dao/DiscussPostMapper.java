package com.sepnine.community.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sepnine.community.entity.DiscussPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/12 20:14
 */
@Mapper
public interface DiscussPostMapper{

    //分页查询讨论的帖子
    List<DiscussPost> selectDiscussPosts(int userId,int offset,int limit);

    //查询帖子的行数。
    // 如果需要动态sql，而且方法只有一个参数，则该参数必须取别名
    //@Param ： 放在dao接口的方法的形参前面， 作为命名参数使用的。使用在xml文件中的#{}
    int selectDiscussPostRows(@Param("userId") int userId);

}
