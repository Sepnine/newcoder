package com.sepnine.community.dao;

import org.springframework.stereotype.Repository;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/10 15:06
 */
@Repository("alphaHibernate") //alphaHibernate是该Bean的名字，不过不写，默认是类名首字母小写
public class AlphaDaoHibernateImpl implements AlphaDao{
    @Override
    public String select() {
        return "Hibernate";
    }
}
