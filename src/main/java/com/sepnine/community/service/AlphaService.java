package com.sepnine.community.service;

import com.sepnine.community.dao.AlphaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/10 16:59
 */
@Service
//@Scope("prototype") singleton是单例的，prototype是多实例的
public class AlphaService {

    @Autowired
    private AlphaDao alphaDao;

    public AlphaService(){
        System.out.println("实例化AlphaService");
    }

    @PostConstruct  //在构造器之后会调用该初始胡函数
    public void init(){
        System.out.println("初始化AlphaService");
    }

    //在销毁之前调用该方法
    @PreDestroy
    public void destory(){
        System.out.println("销毁AlphaService");
    }

    public String find(){
        return alphaDao.select();
    }
}
