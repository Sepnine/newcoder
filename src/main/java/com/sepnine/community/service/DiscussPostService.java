package com.sepnine.community.service;

import com.sepnine.community.dao.DiscussPostMapper;
import com.sepnine.community.entity.DiscussPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/12 20:38
 */
@Service
public class DiscussPostService {

    @Autowired
    private DiscussPostMapper discussPostMapper;

    public List<DiscussPost> findDiscussPosts(int userId, int offset,int limie){
        return discussPostMapper.selectDiscussPosts(userId,offset,limie);
    }

    public int findDiscussPostRows(int userId){
        return discussPostMapper.selectDiscussPostRows(userId);
    }
}
