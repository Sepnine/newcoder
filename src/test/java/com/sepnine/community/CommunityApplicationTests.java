package com.sepnine.community;

import com.sepnine.community.dao.AlphaDao;
import com.sepnine.community.service.AlphaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
//该注解标识使用Community类的配置
@ContextConfiguration(classes = CommunityApplication.class)
//实现ApplicationContextAware之后，这个类就可以方便获得ApplicationContext中的所有bean
class CommunityApplicationTests implements ApplicationContextAware {
	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext=applicationContext;
	}

	@Test
	public void testApplicationContext(){
		System.out.println(applicationContext);

		//从容器中获取Bean
		//有什么好处? 减少代码耦合度
		AlphaDao alphaDao=applicationContext.getBean(AlphaDao.class);
		System.out.println(alphaDao.select());  //输出Mybatis
		//通过类型和Bean名称获取该类
		alphaDao=applicationContext.getBean("alphaHibernate",AlphaDao.class);
		System.out.println(alphaDao.select());	//输出Hibernate
	}

	@Test
	public void testBeanManager(){
		AlphaService alphaService=applicationContext.getBean(AlphaService.class);
		System.out.println(alphaService);

		alphaService=applicationContext.getBean(AlphaService.class);
		System.out.println(alphaService);
	}

	//获取第三方(作者之外)的Bean
	@Test
	public void testBeanConfig(){
		SimpleDateFormat simpleDateFormat = applicationContext.getBean(SimpleDateFormat.class);
		System.out.println(simpleDateFormat.format(new Date()));
	}

	@Autowired  //自动注入，Spring会将AlphaDao这个属性自动注入给下面的变量
	//@Qualifier("alphaHibernate")  指定注入的是下面接口的哪个类
	private AlphaDao alphaDao;

	@Test
	public void testDi(){
		System.out.println(alphaDao);
	}
}
