package com.sepnine.community;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/13 16:07
 */
@SpringBootTest
//该注解标识使用Community类的配置
@ContextConfiguration(classes = CommunityApplication.class)
//实现ApplicationContextAware之后，这个类就可以方便获得ApplicationContext中的所有bean
public class LoggerTest {

    //传入的参数就是logger的名字
    private static final Logger logger = LoggerFactory.getLogger(LoggerTest.class);

    @Test
    public void testLogger(){
        System.out.println(logger.getName());

        logger.debug("debug");
        logger.info("log info");
        logger.warn("warn log");
        logger.error("error log");
    }
}
