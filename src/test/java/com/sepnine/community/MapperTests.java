package com.sepnine.community;

import com.sepnine.community.dao.DiscussPostMapper;
import com.sepnine.community.dao.UserMapper;
import com.sepnine.community.entity.DiscussPost;
import com.sepnine.community.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;
import java.util.List;

/**
 * @author Sepnine
 * @version 1.0
 * @description: TODO
 * @date 2022/9/12 14:31
 */
@SpringBootTest
//该注解标识使用Community类的配置
@ContextConfiguration(classes = CommunityApplication.class)
//实现ApplicationContextAware之后，这个类就可以方便获得ApplicationContext中的所有bean
public class MapperTests {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DiscussPostMapper discussPostMapper;

    @Test
    public void testSelectUser(){
        User user = userMapper.selectById(101);
        System.out.println(user);

        user = userMapper.selectByName("liubei");
        System.out.println(user);

        user = userMapper.selectByEmail("nowcoder101@sina.com");
        System.out.println(user);
    }

    @Test
    public void testInsertUser(){
        User user = new User();
        user.setUsername("test");
        user.setPassword("123456");
        user.setSalt("abc");
        user.setEmail("test@qq.com");
        user.setHeaderUrl("http://www.nowcoder.con.101.png");
        user.setCreateTime(new Date());
        int i = userMapper.insertUser(user);
        System.out.println(i);
        System.out.println(user.getId());
    }
    @Test
    public void testUpdateUser(){
        int i = userMapper.updateStatus(150, 0);
        System.out.println(i);
        int i1 = userMapper.updateHeader(150, "test@123.com");
        System.out.println(i1);
        int i2 = userMapper.updatePassword(150, "123123");
        System.out.println(i2);
    }
    @Test
    public void testSelectPosts(){
        List<DiscussPost> discussPosts = discussPostMapper.selectDiscussPosts(0, 0, 10);
        discussPosts.forEach(s-> System.out.println(s));

        int i = discussPostMapper.selectDiscussPostRows(0);
        System.out.println(i);
    }


}
